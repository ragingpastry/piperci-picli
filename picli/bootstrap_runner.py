from cookiecutter.main import cookiecutter
from pathlib import Path
from picli import logger
from typing import Optional, Union

LOG = logger.get_logger(__name__)


class BaseBootstrap:
    def bootstrap(
        self,
        project: str,
        template_url: str,
        language: str,
        checkout_branch: str,
        config: Optional[Union[str, Path]] = None,
    ) -> None:
        """
        Bootstraps a project directory with the given name for the given language
        :param: template_url: URL of template to pull. Mutually exclusive with language.
        :param language: language project to pull.
        :param checkout_branch: branch to checkout. Defaults to master
        :param config: path to configuration yml (cookiecutter standard)
        :return:
        """
        LOG.info(f"Bootstrapping project in {project}")
        checkout_repo = (
            f"https://gitlab.com/dreamer-labs/piperci/"
            f"piperci-cookiecutter-{language}.git"
        ) if not template_url else template_url

        if project:
            context = {f"project_name": project}
        else:
            context = None

        if not config:
            cookiecutter(
                checkout_repo,
                checkout=checkout_branch,
                overwrite_if_exists=True,
                no_input=True,
                default_config=True,
                extra_context=context,
            )
        else:
            cookiecutter(
                checkout_repo,
                checkout=checkout_branch,
                overwrite_if_exists=True,
                no_input=True,
                config_file=Path(config),
                extra_context=context,
            )
