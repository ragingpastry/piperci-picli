from typing import Any, Dict, List


def exclusive(
    ctx_params: Dict[Any, Any], exclusive_params: List[str]
) -> bool:
    if sum([1 if ctx_params.get(p) else 0 for p in exclusive_params]) > 1:
        return False
    else:
        return True
