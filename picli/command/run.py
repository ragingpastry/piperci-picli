import click
from picli import logger
from picli.config import BaseConfig

LOG = logger.get_logger(__name__)


@click.command()
@click.option("--stages", help="Comma separated list of stages to run")
@click.option(
    "--clean", is_flag=True, default=False, help="Run PiCli with a clean state file."
)
@click.option(
    "--no-wait",
    is_flag=True,
    default=True,
    help="Wait for PiCli remote job execution to finish and display results",
)
@click.option(
    "--no-validate",
    is_flag=True,
    default=False,
    help="Disable validation for the run."
)
@click.pass_context
def run(context, stages, clean, no_wait, no_validate):
    debug = context.obj.get("args")["debug"]
    config_directory = context.obj.get("args")["config"]
    config = BaseConfig(config_directory, clean_state=clean, debug=debug, wait=no_wait)
    if stages:
        stages_list = stages.split(",")
        sequence = config.get_sequence(stages=stages_list)
    else:
        stages_list = [s.name for s in config.stages]
        sequence = config.get_sequence(stages=stages_list)

    if not no_validate:
        config.validate(sequence)
    config.execute(sequence)
