.. PiperCI-PiCli documentation master file, created by
   sphinx-quickstart on Tue Apr  9 09:59:16 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PiperCI-PiCli's documentation!
============================================

These docs are
OUT OF DATE

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   user/index
   developer/index



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
