import pytest

from picli.command.util import exclusive


@pytest.mark.parametrize(
    "given_params, exclusive_params, result",
    [
        ({"a": "1", "b": "2", "c": "3"}, ["a", "c"], False),
        ({"a": "1", "c": "3"}, ["a", "b"], True),
    ],
)
def test_exclusive(given_params, exclusive_params, result):
    assert exclusive(given_params, exclusive_params) == result
