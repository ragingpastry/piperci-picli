import logging
import zipfile

import mock
import pytest
import responses
import requests
from picli.config import BaseConfig
from picli.stage import Stage
from piperci.gman.exceptions import TaskError
from piperci.storeman.exceptions import StoremanError


@pytest.fixture
def default_stage_fixture():
    default_stage = {
        "name": "default",
        "deps": [],
        "tasks": [{"name": "default", "uri": "/default", "config": {"files": "*"}}],
    }

    return default_stage


@pytest.fixture
def dependent_stage_fixture():
    dependent_stage = {
        "name": "dependent",
        "deps": ["default"],
        "resources": [{"name": "dependent", "uri": "/dependent"}],
        "config": [{"files": "*", "resource": "dependent"}],
    }
    return dependent_stage


@pytest.fixture()
def stages_fixture(default_stage_fixture, dependent_stage_fixture):
    stages = {"stages": [default_stage_fixture, dependent_stage_fixture]}
    return stages


@pytest.fixture
def stage_instance(default_stage_fixture, config_instance):
    stage = Stage(default_stage_fixture, config_instance)
    return stage


def test_stage_contains_config(stage_instance):
    assert isinstance(stage_instance.stage_config, BaseConfig)


def test_stage_name_is_default(stage_instance):
    assert stage_instance.name == "default"


def test_stage_logger_set_debug(mocker, default_stage_fixture, config_instance):
    mock_logger_set_level = mocker.patch("picli.stage.LOG.setLevel")
    config_instance.debug = True
    Stage(default_stage_fixture, config_instance)
    mock_logger_set_level.assert_called_with(logging.DEBUG)


def test_stage_can_add_dependencies(
    stage_instance, default_stage_fixture, config_instance
):
    stages = [Stage(default_stage_fixture, config_instance) for _ in range(1, 5)]
    for stage in stages:
        stage_instance.add_dependency(stage)

    assert all(isinstance(stage, Stage) for stage in stage_instance.dependencies)


def test_wait_on_local_state_missing_stage(stage_instance):

    with pytest.raises(SystemExit):
        stage_instance._wait_on_local_state()

def test_wait_on_local_state_missing_thread(stage_instance):

    stage_instance.stage_config.state.update({"default": {}})

    with pytest.raises(SystemExit):
        stage_instance._wait_on_local_state()


def test_validate_calls_sysexit_with_errors(stage_instance):
    invalid_stage_def = {}
    with mock.patch("picli.stage.stage_schema.validate", mock.MagicMock()):
        with pytest.raises(SystemExit):
            stage_instance._validate_stage_config(invalid_stage_def)


@pytest.mark.usefixtures("baseconfig_patches", "default_read_patches")
def test_dependent_stage_is_not_complete(mocker):
    c = BaseConfig("blah", debug=False)
    c.state = {"validate": {"state": "no"}}
    stage = next(stage for stage in c.stages if stage.name == "style")
    mocker.patch("picli.stage.Stage._check_thread_status", return_value=False)
    assert not stage._is_dependent_stage_state_completed()


@pytest.mark.usefixtures("baseconfig_patches", "default_read_patches")
def test_dependent_stage_is_complete(mocker):
    c = BaseConfig("blah", debug=False)
    c.state = {"validate": {"state": "completed"}}
    stage = next(stage for stage in c.stages if stage.name == "style")
    mocker.patch("picli.stage.Stage._check_thread_status", return_value=True)
    assert stage._is_dependent_stage_state_completed()


def test_check_thread_status_updates_state(mocker, stage_instance):
    mocker.patch("piperci.gman.client.wait_for_thread_id_complete", return_value=True)
    mocker.patch("piperci.gman.client.update_task_id")
    with mock.patch("picli.config.BaseConfig.update_state") as mock_update_task_status:
        stage_instance._check_thread_status("1234")
        mock_update_task_status.assert_called_once()


def test_check_thread_status_failed(mocker, stage_instance):
    mocker.patch(
        "piperci.gman.client.wait_for_thread_id_complete", side_effect=TaskError
    )
    mocker.patch("piperci.gman.client.update_task_id")
    mocker.patch("piperci.gman.client.get_events", return_value={})
    mocker.patch("picli.config.BaseConfig.update_state")
    with pytest.raises(SystemExit):
        stage_instance._check_thread_status("1234", "completed")


def test_check_thread_status_timeout(mocker, stage_instance):
    mocker.patch(
        "piperci.gman.client.wait_for_thread_id_complete", side_effect=TimeoutError
    )
    mocker.patch("piperci.gman.client.update_task_id")
    mocker.patch("piperci.gman.client.get_events", return_value={})
    mocker.patch("picli.config.BaseConfig.update_state")
    with pytest.raises(SystemExit):
        stage_instance._check_thread_status("1234", "completed")


def test_submit_job_updates_gman(mocker, stage_instance):
    stage_instance.stage_config.state = {"default": {"artifacts": {}}}
    mocker.patch("picli.config.BaseConfig.update_state")
    mocker.patch("picli.stage.requests.post")
    with mock.patch("piperci.gman.client.update_task_id") as mock_update:
        stage_instance._submit_job("resource", "task")
        mock_update.assert_called()


def test_submit_job_updates_state(mocker, stage_instance):
    stage_instance.stage_config.state = {"default": {"artifacts": {}}}
    mocker.patch("piperci.gman.client.update_task_id")
    mocker.patch("picli.stage.requests.post")
    with mock.patch("picli.config.BaseConfig.update_state") as mock_update:
        stage_instance._submit_job("resource", "task")
        mock_update.assert_called_once()


@pytest.mark.parametrize("validate", [True, False])
def test_submit_job_fails_with_500_updates_state(mocker, stage_instance, validate):
    stage_instance.stage_config.state = {"default": {"artifacts": {}}}
    mocker.patch(
        "picli.stage.requests.post", side_effect=requests.exceptions.RequestException
    )
    mocker.patch("piperci.gman.client.update_task_id")
    with mock.patch("picli.config.BaseConfig.update_state") as mock_update:
        with pytest.raises(SystemExit):
            stage_instance._submit_job("http://resource_url", "task", validate=validate)
        mock_update.assert_called_once()


@pytest.mark.parametrize("validate", [True, False])
@responses.activate
def test_submit_job_fails_with_request_exception_updates_state(mocker, stage_instance, validate):
    stage_instance.stage_config.state = {"default": {"artifacts": {}}}
    responses.add(responses.POST, "http://resource_url", status=500)
    mocker.patch("piperci.gman.client.update_task_id")
    with mock.patch("picli.config.BaseConfig.update_state") as mock_update:
        with pytest.raises(SystemExit):
            stage_instance._submit_job("http://resource_url", "task", validate=validate)
        mock_update.assert_called_once()


@responses.activate
def test_submit_job_success_updates_gman(
    mocker, stage_instance, dummy_controller_response
):
    stage_instance.stage_config.state = {"default": {"artifacts": {}}}
    responses.add(
        responses.POST,
        "http://resource_url",
        status=200,
        json=dummy_controller_response,
    )
    mock_update = mocker.patch("piperci.gman.client.update_task_id")
    stage_instance._submit_job("http://resource_url", "task")
    mock_update.assert_called()


def test_display_downloads_artifact(mocker, stage_instance, artifact):
    mocker.patch("piperci.artman.artman_client.get_artifact", return_value=[artifact])
    mocker.patch("picli.stage.os.path.basename", return_value="blah")
    mocker.patch("picli.stage.Stage._check_thread_status", return_value=True)
    mocker.patch("builtins.open", new_callable=mock.mock_open)
    mock_download = mocker.patch(
        "piperci.storeman.minio_client.MinioClient.download_file"
    )
    stage_instance.stage_config.state = {
        "default": {"task_id": "blah", "thread_id": "blah"},
        "dependent": {"task_id": "blah", "thread_id": "blah"},
    }
    stage_instance.display()
    mock_download.assert_called_once()


def test_display_warns_no_artifacts(mocker, stage_instance, artifact):
    mocker.patch("piperci.artman.artman_client.get_artifact", return_value=[])
    mocker.patch("picli.stage.os.path.basename", return_value="blah")
    mocker.patch("picli.stage.Stage._check_thread_status", return_value=True)
    mocker.patch("builtins.open", new_callable=mock.mock_open)
    mocker.patch("piperci.storeman.minio_client.MinioClient.download_file")
    mock_log_info = mocker.patch("picli.stage.LOG.warn")
    stage_instance.stage_config.state = {
        "default": {"task_id": "blah", "thread_id": "blah"},
        "dependent": {"task_id": "blah", "thread_id": "blah"},
    }
    stage_instance.display()
    mock_log_info.assert_called_once()



def test_download_downloads_artifact(mocker, stage_instance, artifact):
    mocker.patch("piperci.artman.artman_client.get_artifact", return_value=[artifact])
    mocker.patch("picli.stage.os.path.basename", return_value="blah")
    mocker.patch("picli.stage.Stage._check_thread_status", return_value=True)
    mocker.patch("builtins.open", new_callable=mock.mock_open)
    mock_download = mocker.patch(
        "piperci.storeman.minio_client.MinioClient.download_file"
    )
    stage_instance.stage_config.state = {
        "default": {"task_id": "blah", "thread_id": "blah"},
        "dependent": {"task_id": "blah", "thread_id": "blah"},
    }
    stage_instance.download()
    mock_download.assert_called()


def test_download_warns_no_artifacts(mocker, stage_instance):
    mocker.patch("piperci.artman.artman_client.get_artifact", return_value=[])
    mocker.patch("picli.stage.os.path.basename", return_value="blah")
    mocker.patch("picli.stage.Stage._check_thread_status", return_value=True)
    mocker.patch("builtins.open", new_callable=mock.mock_open)
    mocker.patch("piperci.storeman.minio_client.MinioClient.download_file")
    mock_log_info = mocker.patch("picli.stage.LOG.warn")
    stage_instance.stage_config.state = {
        "default": {"task_id": "blah", "thread_id": "blah"},
        "dependent": {"task_id": "blah", "thread_id": "blah"},
    }
    stage_instance.download()
    mock_log_info.assert_called()


def test_download_exits_no_state(mocker, stage_instance):
    mocker.patch(
        "piperci.artman.artman_client.get_artifact", return_value=[mock.MagicMock()]
    )
    mocker.patch("picli.stage.os.path.basename", return_value="blah")
    mocker.patch("picli.stage.Stage._check_thread_status", return_value=True)
    mocker.patch("builtins.open", new_callable=mock.mock_open)
    mock_download = mocker.patch(
        "piperci.storeman.minio_client.MinioClient.download_file"
    )
    stage_instance.stage_config.state = {
        "default": {"task_id": "blah", "thread_id": "blah"},
        "dependent": {"task_id": "blah", "thread_id": "blah"},
    }
    stage_instance.stage_config.state = {}
    with pytest.raises(SystemExit):
        stage_instance.download()


def test_download_exits_no_thread_id_in_state(mocker, stage_instance):
    mocker.patch(
        "piperci.artman.artman_client.get_artifact", return_value=[mock.MagicMock()]
    )
    mocker.patch("picli.stage.os.path.basename", return_value="blah")
    mocker.patch("picli.stage.Stage._check_thread_status", return_value=True)
    mocker.patch("builtins.open", new_callable=mock.mock_open)
    mock_download = mocker.patch(
        "piperci.storeman.minio_client.MinioClient.download_file"
    )
    stage_instance.stage_config.state = {
        "default": {"task_id": "blah"},
        "dependent": {"task_id": "blah", "thread_id": "blah"},
    }
    with pytest.raises(SystemExit):
        stage_instance.download()


def test_download_exits_thread_not_complete(mocker, stage_instance):
    mocker.patch(
        "piperci.artman.artman_client.get_artifact", return_value=[mock.MagicMock()]
    )
    mocker.patch("picli.stage.os.path.basename", return_value="blah")
    mocker.patch("picli.stage.Stage._check_thread_status", side_effect=TaskError)
    mocker.patch("builtins.open", new_callable=mock.mock_open)
    mock_download = mocker.patch(
        "piperci.storeman.minio_client.MinioClient.download_file"
    )
    stage_instance.stage_config.state = {
        "default": {"task_id": "blah", "thread_id": "blah"},
        "dependent": {"task_id": "blah", "thread_id": "blah"},
    }
    with pytest.raises(SystemExit):
        stage_instance.download()


@pytest.mark.usefixtures("baseconfig_patches", "default_read_patches")
def test_display_shows_already_downloaded_file(mocker, stage_instance):
    mock_download = mocker.patch("picli.stage.Stage.download")
    mocker.patch("builtins.open", new_callable=mock.mock_open)
    mocker.patch("picli.stage.os.path.exists", return_value=True)
    stage_instance.stage_config.state["default"] = {}
    stage_instance.stage_config.state["default"]["thread_id"] = "1234"
    mocker.patch("picli.stage.Stage._wait_on_local_state")
    artifact_response = [
        {
            'task': {
                'project': 'python_project',
                'parent_id': '942ea1f6-c0a3-4012-a953-3f50f25137e3', 
                'run_id': '87c6337f-a45c-4c63-aaa5-a18a913ebc16',
                'caller': 'flake8',
                'task_id': '6149bd27-26c2-4a19-a9f4-ffd4c58aa8e0',
                'thread_id': '942ea1f6-c0a3-4012-a953-3f50f25137e3'
            },
            'status': 'unknown', 
            'artifact_id': '7cabad93-9970-4538-985a-6c90842772d5',
            'event_id': '2709461f-6599-4f43-a2ab-1d41ea64c19d',
            'sri': 'sha256-PG+JC2eRQDkKNJhFuGmgA2OyPn3ypfyawVAI9YRrfMM=',
            'type': 'stdout',
            'uri': 'minio://minio:9000/run-87c6337f-a45c-4c63-aaa5-a18a913ebc16/stdout/style/6149bd27-2-out'
        }
    ]
    mocker.patch("picli.stage.artman_client.get_artifact", return_value=artifact_response)


    stage_instance.display()

    assert not mock_download.called, "Display should not redownload logs"


@pytest.mark.usefixtures("baseconfig_patches", "default_read_patches")
def test_display_downloads_stdout(mocker, stage_instance, artifact):
    artifact_response = [
        {
            'task': {
                'project': 'python_project',
                'parent_id': '942ea1f6-c0a3-4012-a953-3f50f25137e3', 
                'run_id': '87c6337f-a45c-4c63-aaa5-a18a913ebc16',
                'caller': 'flake8',
                'task_id': '6149bd27-26c2-4a19-a9f4-ffd4c58aa8e0',
                'thread_id': '942ea1f6-c0a3-4012-a953-3f50f25137e3'
            },
            'status': 'unknown', 
            'artifact_id': '7cabad93-9970-4538-985a-6c90842772d5',
            'event_id': '2709461f-6599-4f43-a2ab-1d41ea64c19d',
            'sri': 'sha256-PG+JC2eRQDkKNJhFuGmgA2OyPn3ypfyawVAI9YRrfMM=',
            'type': 'stdout',
            'uri': 'minio://minio:9000/run-87c6337f-a45c-4c63-aaa5-a18a913ebc16/stdout/style/6149bd27-2-out'
        }
    ]
    mocker.patch("picli.stage.artman_client.get_artifact", return_value=artifact_response)
    mock_download = mocker.patch("picli.stage.Stage.download")
    mocker.patch("picli.stage.Stage._wait_on_local_state")
    stage_instance.stage_config.state["default"] = {}
    stage_instance.stage_config.state["default"]["thread_id"] = "1234"
    mocker.patch("builtins.open", new_callable=mock.mock_open)

    stage_instance.display()

    mock_download.assert_called_once_with(file_type="stdout")


def test_zip_file_creates_file(stage_instance, tmpdir):
    temp_directory = tmpdir.mkdir("sub")
    temp_file = temp_directory.join("hello.txt")
    temp_file.write("content")
    stage_instance.stage_config.base_path = str(temp_directory)
    print(stage_instance.stage_config.base_path)
    zip_file = stage_instance._zip_project(str(tmpdir))
    assert isinstance(zip_file, zipfile.ZipFile)


def test_zip_file_ignores_state_directory(stage_instance, tmpdir):
    project_directory = tmpdir.mkdir(stage_instance.stage_config.project_name)
    state_directory = (
        project_directory.mkdir("piperci.d").mkdir("default").mkdir("state")
    )
    state_file = state_directory.join("state.yml")
    state_file.write("this_should_not_be_in_zip")
    stage_instance.stage_config.base_path = str(project_directory)
    zip_file = stage_instance._zip_project(str(tmpdir))

    assert not len(zip_file.namelist())


@pytest.mark.usefixtures("default_stage_create_project_patches")
def test_create_project_artifact_calls_upload_file_if_artifact_not_exists(
    mocker, stage_instance
):
    stage_instance.stage_config.state = {"default": {"client_task_id": "1234"}}
    mocker.patch("picli.stage.artman_client.check_artifact_exists", return_value=False)
    mocker.patch("picli.stage.artman_client.post_artifact")
    mock_upload = mocker.patch(
        "piperci.storeman.minio_client.MinioClient.upload_file",
        return_value=mock.MagicMock(),
    )
    stage_instance._create_project_artifact()
    mock_upload.assert_called_once()


@pytest.mark.usefixtures("default_stage_create_project_patches")
def test_create_project_artfact_sysexists(mocker, stage_instance):
    stage_instance.stage_config.state = {"default": {"client_task_id": "1234"}}
    mocker.patch("picli.stage.artman_client.check_artifact_exists", return_value=False)
    mocker.patch("picli.stage.artman_client.post_artifact")
    mock_upload = mocker.patch(
        "piperci.storeman.minio_client.MinioClient.upload_file",
        side_effect=StoremanError,
    )
    with pytest.raises(SystemExit):
        stage_instance._create_project_artifact()


@pytest.mark.usefixtures("default_stage_create_project_patches")
def test_create_project_artifact_updates_state_if_artifact_exists(
    mocker, stage_instance
):
    stage_instance.stage_config.state = {"default": {"client_task_id": "1234"}}
    mocker.patch("picli.stage.artman_client.check_artifact_exists", return_value=False)
    mocker.patch(
        "piperci.artman.artman_client.get_artifact", return_value=[{"uri": "1234"}]
    )
    mock_upload = mocker.patch(
        "piperci.artman.artman_client.check_artifact_exists", return_value=True
    )
    mock_update_state = mocker.patch("picli.config.BaseConfig.update_state")
    stage_instance._create_project_artifact()
    mock_upload.assert_called_once()
    mock_update_state.assert_called_once()


@pytest.mark.usefixtures("default_stage_create_project_patches")
def test_create_project_artifact_adds_sri_to_state(mocker, stage_instance):
    stage_instance.stage_config.state = {"default": {"client_task_id": "1234"}}
    mocker.patch("picli.stage.artman_client.check_artifact_exists", return_value=False)
    mocker.patch(
        "piperci.artman.artman_client.get_artifact", return_value=[{"uri": "1234"}]
    )
    mock_upload = mocker.patch(
        "piperci.artman.artman_client.check_artifact_exists", return_value=True
    )
    mock_update_state = mocker.patch("picli.config.BaseConfig.update_state")
    stage_instance._create_project_artifact()
    mock_upload.assert_called_once()

    assert "artifact_sri" in [
        key
        for _, value in mock_update_state.call_args[0][0]["default"][
            "artifacts"
        ].items()
        for key, _ in value.items()
    ]


@pytest.mark.usefixtures("default_stage_create_project_patches")
@pytest.mark.parametrize("artifact_exists", ["True", "False"])
def test_create_project_artifact_handles_malformed_artifact(
    artifact_exists, mocker, stage_instance
):
    stage_instance.stage_config.state = {"default": {"client_task_id": "1234"}}
    mocker.patch("picli.stage.artman_client.check_artifact_exists", return_value=False)
    mocker.patch("piperci.artman.artman_client.get_artifact", return_value=[])
    mock_upload = mocker.patch(
        "piperci.artman.artman_client.check_artifact_exists",
        return_value=artifact_exists,
    )
    mock_update_state = mocker.patch("picli.config.BaseConfig.update_state")
    with pytest.raises(SystemExit):
        stage_instance._create_project_artifact()


@pytest.mark.usefixtures("default_stage_create_project_patches")
def test_create_project_artifact_handles_malformed_state(mocker, stage_instance):
    stage_instance.stage_config.state = {"default": {"malformed": "1234"}}
    mocker.patch("picli.stage.artman_client.check_artifact_exists", return_value=False)
    mocker.patch("piperci.storeman.minio_client.MinioClient.upload_file")
    mocker.patch("piperci.artman.artman_client.get_artifact", return_value=[])
    mock_upload = mocker.patch(
        "piperci.artman.artman_client.check_artifact_exists", return_value=False
    )
    mock_update_state = mocker.patch("picli.config.BaseConfig.update_state")
    with pytest.raises(SystemExit):
        stage_instance._create_project_artifact()


def test_execute_skips_stage_if_complete(stage_instance):
    stage_instance.stage_config.state = {"default": {"state": "completed"}}
    assert stage_instance.execute() is None


@pytest.mark.usefixtures("default_stage_execute_patches")
def test_execute_requests_task_id(mocker, stage_instance):
    mock_request_task = mocker.patch("piperci.gman.client.request_new_task_id")
    stage_instance.execute()
    mock_request_task.assert_called_once()


@pytest.mark.usefixtures("default_stage_execute_patches")
def test_execute_request_task_id(mocker, stage_instance):
    mocker.patch(
        "piperci.gman.client.request_new_task_id", side_effect=requests.RequestException
    )
    with pytest.raises(SystemExit):
        stage_instance.execute()


@pytest.mark.usefixtures("default_stage_execute_patches")
def test_execute_checks_dependent_stages(mocker, stage_instance):
    check_dep = mocker.patch(
        "picli.stage.Stage._is_dependent_stage_state_completed", return_value=True
    )
    stage_instance.execute()
    check_dep.assert_called_once()


@pytest.mark.usefixtures("default_stage_execute_patches")
def test_execute_fails_dependent_stages(mocker, stage_instance):
    mocker.patch(
        "picli.stage.Stage._is_dependent_stage_state_completed", return_value=False
    )
    with pytest.raises(SystemExit):
        stage_instance.execute()


@pytest.mark.usefixtures("default_stage_execute_patches")
def test_execute_updates_state(mocker, stage_instance):
    mock_update_state = mocker.patch("picli.config.BaseConfig.update_state")
    stage_instance.execute()
    mock_update_state.assert_called_once_with(
        {"default": {"state": "started", "client_task_id": "1234"}}
    )


@pytest.mark.usefixtures("default_stage_execute_patches")
def test_execute_calls_submit_job_with_resource_url(
    mocker, stage_instance, dummy_controller_response
):
    mocker.patch("picli.stage.Stage._create_project_artifact", return_value="artifacts")
    mocker.patch(
        "piperci.gman.client.request_new_task_id",
        return_value=dummy_controller_response,
    )

    expected_resource_configs = {"files": "*"}

    mock_submit_job = mocker.patch("picli.stage.Stage._submit_job")
    stage_instance.execute()
    mock_submit_job.assert_called_once_with(
        "http://172.17.0.1:8000/default", "1234", config=expected_resource_configs
    )


@pytest.mark.usefixtures("default_stage_execute_patches")
def test_execute_invalid_resource_config(stage_instance):
    stage_instance.tasks = {}
    with pytest.raises(SystemExit):
        stage_instance.execute()


@pytest.mark.usefixtures("default_stage_execute_patches")
def test_validate_invalid_resource_config(stage_instance):
    stage_instance.tasks = {}
    with pytest.raises(SystemExit):
        stage_instance.validate()


@pytest.mark.usefixtures("default_stage_execute_patches")
def test_execute_wait_calls_display(mocker, stage_instance):
    mock_display = mocker.patch("picli.stage.Stage.display")
    stage_instance.execute(wait=True)
    mock_display.assert_called_once()


@pytest.mark.usefixtures("default_stage_execute_patches")
def test_execute_with_debug(mocker, stage_instance):
    mock_log_info = mocker.patch("picli.stage.LOG.debug")
    stage_instance.stage_config.debug = True
    stage_instance.execute()
    mock_log_info.assert_called()


@pytest.mark.usefixtures("default_stage_execute_patches")
def test_validate_calls_submit_validation_job_with_resource_url(
    mocker, stage_instance, dummy_controller_response
):
    expected_resource_configs = {"files": "*"}
    return_value = mocker.Mock()
    return_value.status_code = 200

    mock_submit_job = mocker.patch(
        "picli.stage.Stage._submit_job", return_value=return_value
    )
    stage_instance.validate()
    mock_submit_job.assert_called_once_with(
        "http://172.17.0.1:8000/default",
        "1234",
        config=expected_resource_configs,
        validate=True,
    )


@responses.activate
def test_submit_validation_job_returns_results(mocker, stage_instance):
    stage_instance.stage_config.state = {"default": {"artifacts": {}}}
    mocker.patch("picli.stage.Stage._is_dependent_stage_state_completed")
    mocker.patch("picli.config.BaseConfig.update_state")
    mocker.patch("picli.stage.Stage._create_project_artifact")
    mocker.patch("picli.stage.Stage._check_thread_status")
    mocker.patch(
        "piperci.gman.client.request_new_task_id",
        return_value={"task": {"task_id": "1234"}},
    )
    responses.add(responses.POST, "http://resource_url", status=200)
    mocker.patch("piperci.gman.client.update_task_id")
    assert (
        stage_instance._submit_job(
            "http://resource_url", "task", validate=True
        ).status_code
        == 200
    )

@responses.activate
def test_submit_validation_job_http_failure_updates_state(mocker, stage_instance):
    stage_instance.stage_config.state
    stage_instance.stage_config.state = {"default": {"artifacts": {}}}
    mocker.patch("picli.stage.Stage._is_dependent_stage_state_completed")
    update_state = mocker.patch("picli.config.BaseConfig.update_state")
    mocker.patch("picli.stage.Stage._create_project_artifact")
    mocker.patch("picli.stage.Stage._check_thread_status")
    mocker.patch(
        "piperci.gman.client.request_new_task_id",
        return_value={"task": {"task_id": "1234"}},
    )
    responses.add(responses.POST, "http://resource_url", status=500)
    mocker.patch("piperci.gman.client.update_task_id")
    with pytest.raises(SystemExit):
        stage_instance._submit_job(
                "http://resource_url", "task", validate=True
            )
        update_state.assert_called_once()



@pytest.mark.parametrize("return_value", [(200, "succeeded"), (500, "failed")])
@pytest.mark.usefixtures("default_stage_execute_patches")
def test_validate_calls_info_failure(return_value, mocker, stage_instance):
    stage_instance.stage_config.state = {"default": {"artifacts": {}}}
    mock_return = mocker.Mock()
    mock_return.status_code = return_value[0]
    mocker.patch("picli.stage.Stage._submit_job", return_value=mock_return)
    mock_log_info = mocker.patch("picli.stage.LOG.info")
    stage_instance.validate()

    mock_log_info.assert_called_with(f"Validation {return_value[1]}.")


@pytest.mark.usefixtures("default_stage_execute_patches")
def test_validate_request_task_id_fails(mocker, stage_instance):
    mocker.patch(
        "piperci.gman.client.request_new_task_id", side_effect=requests.RequestException
    )
    with pytest.raises(SystemExit):
        stage_instance.validate()
