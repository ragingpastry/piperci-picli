# 1.0.0 (2020-03-17)


### Bug Fixes

* catch errors and handle nicely ([4c4f211](https://gitlab.com/ragingpastry/piperci-picli/commit/4c4f211))
* correct display function call ([3473c59](https://gitlab.com/ragingpastry/piperci-picli/commit/3473c59))
* Fix flag for only_validate ([a3305d8](https://gitlab.com/ragingpastry/piperci-picli/commit/a3305d8))
* fix unit tests for validation ([75a9234](https://gitlab.com/ragingpastry/piperci-picli/commit/75a9234))
* Improve dependency parsing in setup.py ([c3d1f4b](https://gitlab.com/ragingpastry/piperci-picli/commit/c3d1f4b))
* Mark task as complete after submission ([b830a30](https://gitlab.com/ragingpastry/piperci-picli/commit/b830a30))
* Pull artifact URIs from stages ([78dc261](https://gitlab.com/ragingpastry/piperci-picli/commit/78dc261))
* remove old flags from CMD ([6ce444d](https://gitlab.com/ragingpastry/piperci-picli/commit/6ce444d))
* request task with validation ([aac02fb](https://gitlab.com/ragingpastry/piperci-picli/commit/aac02fb))
* send sri hash to remote, not urlsafe sri ([5e49c1c](https://gitlab.com/ragingpastry/piperci-picli/commit/5e49c1c))


### Features

* Add ability to download all logs ([ad0b0b1](https://gitlab.com/ragingpastry/piperci-picli/commit/ad0b0b1))
* Add bootstrap python (default) project ([e447ccf](https://gitlab.com/ragingpastry/piperci-picli/commit/e447ccf)), closes [#22](https://gitlab.com/ragingpastry/piperci-picli/issues/22)
* add cmd to docker ([94583e7](https://gitlab.com/ragingpastry/piperci-picli/commit/94583e7))
* Add sri to artifact data ([0925173](https://gitlab.com/ragingpastry/piperci-picli/commit/0925173))
* add template-url parm to bootstrap ([3aea0e9](https://gitlab.com/ragingpastry/piperci-picli/commit/3aea0e9)), closes [#35](https://gitlab.com/ragingpastry/piperci-picli/issues/35) [#36](https://gitlab.com/ragingpastry/piperci-picli/issues/36)
* add validation command ([33056fe](https://gitlab.com/ragingpastry/piperci-picli/commit/33056fe))
* change artifact json and task_id ([5bc8d74](https://gitlab.com/ragingpastry/piperci-picli/commit/5bc8d74))
* initial release ([e01823e](https://gitlab.com/ragingpastry/piperci-picli/commit/e01823e))
* Move config options for simplicity ([3e23fb0](https://gitlab.com/ragingpastry/piperci-picli/commit/3e23fb0))
* tag source artifact type in Minio ([e3fb3db](https://gitlab.com/ragingpastry/piperci-picli/commit/e3fb3db))

# 1.0.0 (2020-03-17)


### Bug Fixes

* catch errors and handle nicely ([4c4f211](https://gitlab.com/ragingpastry/piperci-picli/commit/4c4f211))
* correct display function call ([3473c59](https://gitlab.com/ragingpastry/piperci-picli/commit/3473c59))
* Fix flag for only_validate ([a3305d8](https://gitlab.com/ragingpastry/piperci-picli/commit/a3305d8))
* fix unit tests for validation ([75a9234](https://gitlab.com/ragingpastry/piperci-picli/commit/75a9234))
* Improve dependency parsing in setup.py ([c3d1f4b](https://gitlab.com/ragingpastry/piperci-picli/commit/c3d1f4b))
* Mark task as complete after submission ([b830a30](https://gitlab.com/ragingpastry/piperci-picli/commit/b830a30))
* Pull artifact URIs from stages ([78dc261](https://gitlab.com/ragingpastry/piperci-picli/commit/78dc261))
* remove old flags from CMD ([6ce444d](https://gitlab.com/ragingpastry/piperci-picli/commit/6ce444d))
* request task with validation ([aac02fb](https://gitlab.com/ragingpastry/piperci-picli/commit/aac02fb))
* send sri hash to remote, not urlsafe sri ([5e49c1c](https://gitlab.com/ragingpastry/piperci-picli/commit/5e49c1c))


### Features

* Add ability to download all logs ([ad0b0b1](https://gitlab.com/ragingpastry/piperci-picli/commit/ad0b0b1))
* Add bootstrap python (default) project ([e447ccf](https://gitlab.com/ragingpastry/piperci-picli/commit/e447ccf)), closes [#22](https://gitlab.com/ragingpastry/piperci-picli/issues/22)
* add cmd to docker ([94583e7](https://gitlab.com/ragingpastry/piperci-picli/commit/94583e7))
* Add sri to artifact data ([0925173](https://gitlab.com/ragingpastry/piperci-picli/commit/0925173))
* add template-url parm to bootstrap ([3aea0e9](https://gitlab.com/ragingpastry/piperci-picli/commit/3aea0e9)), closes [#35](https://gitlab.com/ragingpastry/piperci-picli/issues/35) [#36](https://gitlab.com/ragingpastry/piperci-picli/issues/36)
* add validation command ([33056fe](https://gitlab.com/ragingpastry/piperci-picli/commit/33056fe))
* change artifact json and task_id ([5bc8d74](https://gitlab.com/ragingpastry/piperci-picli/commit/5bc8d74))
* initial release ([e01823e](https://gitlab.com/ragingpastry/piperci-picli/commit/e01823e))
* Move config options for simplicity ([3e23fb0](https://gitlab.com/ragingpastry/piperci-picli/commit/3e23fb0))
* tag source artifact type in Minio ([e3fb3db](https://gitlab.com/ragingpastry/piperci-picli/commit/e3fb3db))

# 1.0.0 (2020-03-17)


### Bug Fixes

* catch errors and handle nicely ([4c4f211](https://gitlab.com/ragingpastry/piperci-picli/commit/4c4f211))
* correct display function call ([3473c59](https://gitlab.com/ragingpastry/piperci-picli/commit/3473c59))
* Fix flag for only_validate ([a3305d8](https://gitlab.com/ragingpastry/piperci-picli/commit/a3305d8))
* fix unit tests for validation ([75a9234](https://gitlab.com/ragingpastry/piperci-picli/commit/75a9234))
* Improve dependency parsing in setup.py ([c3d1f4b](https://gitlab.com/ragingpastry/piperci-picli/commit/c3d1f4b))
* Mark task as complete after submission ([b830a30](https://gitlab.com/ragingpastry/piperci-picli/commit/b830a30))
* Pull artifact URIs from stages ([78dc261](https://gitlab.com/ragingpastry/piperci-picli/commit/78dc261))
* remove old flags from CMD ([6ce444d](https://gitlab.com/ragingpastry/piperci-picli/commit/6ce444d))
* request task with validation ([aac02fb](https://gitlab.com/ragingpastry/piperci-picli/commit/aac02fb))
* send sri hash to remote, not urlsafe sri ([5e49c1c](https://gitlab.com/ragingpastry/piperci-picli/commit/5e49c1c))


### Features

* Add ability to download all logs ([ad0b0b1](https://gitlab.com/ragingpastry/piperci-picli/commit/ad0b0b1))
* Add bootstrap python (default) project ([e447ccf](https://gitlab.com/ragingpastry/piperci-picli/commit/e447ccf)), closes [#22](https://gitlab.com/ragingpastry/piperci-picli/issues/22)
* add cmd to docker ([94583e7](https://gitlab.com/ragingpastry/piperci-picli/commit/94583e7))
* Add sri to artifact data ([0925173](https://gitlab.com/ragingpastry/piperci-picli/commit/0925173))
* add template-url parm to bootstrap ([3aea0e9](https://gitlab.com/ragingpastry/piperci-picli/commit/3aea0e9)), closes [#35](https://gitlab.com/ragingpastry/piperci-picli/issues/35) [#36](https://gitlab.com/ragingpastry/piperci-picli/issues/36)
* add validation command ([33056fe](https://gitlab.com/ragingpastry/piperci-picli/commit/33056fe))
* change artifact json and task_id ([5bc8d74](https://gitlab.com/ragingpastry/piperci-picli/commit/5bc8d74))
* initial release ([e01823e](https://gitlab.com/ragingpastry/piperci-picli/commit/e01823e))
* Move config options for simplicity ([3e23fb0](https://gitlab.com/ragingpastry/piperci-picli/commit/3e23fb0))
* tag source artifact type in Minio ([e3fb3db](https://gitlab.com/ragingpastry/piperci-picli/commit/e3fb3db))

# 1.0.0 (2020-03-17)


### Bug Fixes

* catch errors and handle nicely ([4c4f211](https://gitlab.com/ragingpastry/piperci-picli/commit/4c4f211))
* correct display function call ([3473c59](https://gitlab.com/ragingpastry/piperci-picli/commit/3473c59))
* Fix flag for only_validate ([a3305d8](https://gitlab.com/ragingpastry/piperci-picli/commit/a3305d8))
* fix unit tests for validation ([75a9234](https://gitlab.com/ragingpastry/piperci-picli/commit/75a9234))
* Improve dependency parsing in setup.py ([c3d1f4b](https://gitlab.com/ragingpastry/piperci-picli/commit/c3d1f4b))
* Mark task as complete after submission ([b830a30](https://gitlab.com/ragingpastry/piperci-picli/commit/b830a30))
* Pull artifact URIs from stages ([78dc261](https://gitlab.com/ragingpastry/piperci-picli/commit/78dc261))
* remove old flags from CMD ([6ce444d](https://gitlab.com/ragingpastry/piperci-picli/commit/6ce444d))
* request task with validation ([aac02fb](https://gitlab.com/ragingpastry/piperci-picli/commit/aac02fb))
* send sri hash to remote, not urlsafe sri ([5e49c1c](https://gitlab.com/ragingpastry/piperci-picli/commit/5e49c1c))


### Features

* Add ability to download all logs ([ad0b0b1](https://gitlab.com/ragingpastry/piperci-picli/commit/ad0b0b1))
* Add bootstrap python (default) project ([e447ccf](https://gitlab.com/ragingpastry/piperci-picli/commit/e447ccf)), closes [#22](https://gitlab.com/ragingpastry/piperci-picli/issues/22)
* add cmd to docker ([94583e7](https://gitlab.com/ragingpastry/piperci-picli/commit/94583e7))
* Add sri to artifact data ([0925173](https://gitlab.com/ragingpastry/piperci-picli/commit/0925173))
* add template-url parm to bootstrap ([3aea0e9](https://gitlab.com/ragingpastry/piperci-picli/commit/3aea0e9)), closes [#35](https://gitlab.com/ragingpastry/piperci-picli/issues/35) [#36](https://gitlab.com/ragingpastry/piperci-picli/issues/36)
* add validation command ([33056fe](https://gitlab.com/ragingpastry/piperci-picli/commit/33056fe))
* change artifact json and task_id ([5bc8d74](https://gitlab.com/ragingpastry/piperci-picli/commit/5bc8d74))
* initial release ([e01823e](https://gitlab.com/ragingpastry/piperci-picli/commit/e01823e))
* Move config options for simplicity ([3e23fb0](https://gitlab.com/ragingpastry/piperci-picli/commit/3e23fb0))
* tag source artifact type in Minio ([e3fb3db](https://gitlab.com/ragingpastry/piperci-picli/commit/e3fb3db))

## [1.9.5](https://gitlab.com/dreamer-labs/piperci/piperci-picli/compare/v1.9.4...v1.9.5) (2020-02-14)


### Bug Fixes

* remove old flags from CMD ([6ce444d](https://gitlab.com/dreamer-labs/piperci/piperci-picli/commit/6ce444d))

## [1.9.4](https://gitlab.com/dreamer-labs/piperci/piperci-picli/compare/v1.9.3...v1.9.4) (2020-02-14)


### Bug Fixes

* correct display function call ([3473c59](https://gitlab.com/dreamer-labs/piperci/piperci-picli/commit/3473c59))

## [1.9.3](https://gitlab.com/dreamer-labs/piperci/piperci-picli/compare/v1.9.2...v1.9.3) (2020-02-03)


### Bug Fixes

* Fix flag for only_validate ([a3305d8](https://gitlab.com/dreamer-labs/piperci/piperci-picli/commit/a3305d8))
* Mark task as complete after submission ([b830a30](https://gitlab.com/dreamer-labs/piperci/piperci-picli/commit/b830a30))

## [1.9.2](https://gitlab.com/dreamer-labs/piperci/piperci-picli/compare/v1.9.1...v1.9.2) (2020-01-22)


### Bug Fixes

* send sri hash to remote, not urlsafe sri ([5e49c1c](https://gitlab.com/dreamer-labs/piperci/piperci-picli/commit/5e49c1c))

## [1.9.1](https://gitlab.com/dreamer-labs/piperci/piperci-picli/compare/v1.9.0...v1.9.1) (2020-01-21)


### Bug Fixes

* fix unit tests for validation ([75a9234](https://gitlab.com/dreamer-labs/piperci/piperci-picli/commit/75a9234))
* request task with validation ([aac02fb](https://gitlab.com/dreamer-labs/piperci/piperci-picli/commit/aac02fb))

# [1.9.0](https://gitlab.com/dreamer-labs/piperci/piperci-picli/compare/v1.8.0...v1.9.0) (2020-01-13)


### Bug Fixes

* Improve dependency parsing in setup.py ([c3d1f4b](https://gitlab.com/dreamer-labs/piperci/piperci-picli/commit/c3d1f4b))


### Features

* change artifact json and task_id ([5bc8d74](https://gitlab.com/dreamer-labs/piperci/piperci-picli/commit/5bc8d74))

# [1.8.0](https://gitlab.com/dreamer-labs/piperci/piperci-picli/compare/v1.7.0...v1.8.0) (2020-01-03)


### Features

* add validation command ([33056fe](https://gitlab.com/dreamer-labs/piperci/piperci-picli/commit/33056fe))

# [1.7.0](https://gitlab.com/dreamer-labs/piperci/piperci-picli/compare/v1.6.0...v1.7.0) (2019-12-16)


### Features

* Move config options for simplicity ([3e23fb0](https://gitlab.com/dreamer-labs/piperci/piperci-picli/commit/3e23fb0))

# [1.6.0](https://gitlab.com/dreamer-labs/piperci/piperci-picli/compare/v1.5.0...v1.6.0) (2019-10-25)


### Features

* Add sri to artifact data ([0925173](https://gitlab.com/dreamer-labs/piperci/piperci-picli/commit/0925173))

# [1.5.0](https://gitlab.com/dreamer-labs/piperci/piperci-picli/compare/v1.4.0...v1.5.0) (2019-10-21)


### Features

* Add ability to download all logs ([ad0b0b1](https://gitlab.com/dreamer-labs/piperci/piperci-picli/commit/ad0b0b1))

# [1.4.0](https://gitlab.com/dreamer-labs/piperci/piperci-picli/compare/v1.3.0...v1.4.0) (2019-10-16)


### Features

* tag source artifact type in Minio ([e3fb3db](https://gitlab.com/dreamer-labs/piperci/piperci-picli/commit/e3fb3db))

# [1.3.0](https://gitlab.com/dreamer-labs/piperci/piperci-picli/compare/v1.2.1...v1.3.0) (2019-09-13)


### Features

* add template-url parm to bootstrap ([3aea0e9](https://gitlab.com/dreamer-labs/piperci/piperci-picli/commit/3aea0e9)), closes [#35](https://gitlab.com/dreamer-labs/piperci/piperci-picli/issues/35) [#36](https://gitlab.com/dreamer-labs/piperci/piperci-picli/issues/36)

## [1.2.1](https://gitlab.com/dreamer-labs/piperci/piperci-picli/compare/v1.2.0...v1.2.1) (2019-09-11)


### Bug Fixes

* catch errors and handle nicely ([4c4f211](https://gitlab.com/dreamer-labs/piperci/piperci-picli/commit/4c4f211))
* Pull artifact URIs from stages ([78dc261](https://gitlab.com/dreamer-labs/piperci/piperci-picli/commit/78dc261))

# [1.2.0](https://gitlab.com/dreamer-labs/piperci/piperci-picli/compare/v1.1.0...v1.2.0) (2019-08-23)


### Features

* add cmd to docker ([94583e7](https://gitlab.com/dreamer-labs/piperci/piperci-picli/commit/94583e7))

# [1.1.0](https://gitlab.com/dreamer-labs/piperci/piperci-picli/compare/v1.0.0...v1.1.0) (2019-08-07)


### Features

* Add bootstrap python (default) project ([e447ccf](https://gitlab.com/dreamer-labs/piperci/piperci-picli/commit/e447ccf)), closes [#22](https://gitlab.com/dreamer-labs/piperci/piperci-picli/issues/22)

# 1.0.0 (2019-08-07)


### Features

* initial release ([e01823e](https://gitlab.com/dreamer-labs/piperci/piperci-picli/commit/e01823e))
